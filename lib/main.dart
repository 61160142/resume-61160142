import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

Row _CreateInfo(String text1, String text2, double size) {
  return Row(
    children: [
      Container(
        padding: const EdgeInsets.only(
          right: 8,
        ),
        child: Text(
          '$text1 :',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: size),
          softWrap: true,
        ),
      ),
      Text(
        text2,
        style: TextStyle(color: Colors.grey[500], fontSize: size),
        softWrap: true,
      )
    ],
  );
}

Widget bio1 = Container(
  padding: const EdgeInsets.only(
    top: 20,
  ),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      _CreateInfo('ชื่อ', 'ธนพงษ์ ชดช้อย', 20),
      _CreateInfo('ชื่อเล่น', 'ยุมอัต', 20),
    ],
  ),
);

Widget bio2 = Container(
  padding: const EdgeInsets.only(
    top: 10,
    bottom: 20,
  ),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      _CreateInfo('เพศ', 'ชาย', 20),
      _CreateInfo('อายุ', '21 ปี', 20),
    ],
  ),
);
Widget profile(String bio, String info, double size) {
  return Container(
    padding: const EdgeInsets.only(
      top: 10,
      left: 50,
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        _CreateInfo('$bio', '$info', size),
      ],
    ),
  );
}

Widget section(String img, String text) {
  return Container(
    padding: const EdgeInsets.only(
      top: 20,
    ),
    child: Row(
      children: [
        Image.network(
          '$img',
          height: 40,
        ),
        Text(
          ' $text',
          style: TextStyle(fontSize: 20),
        ),
      ],
    ),
  );
}

Widget skillList = Container(
  padding: const EdgeInsets.only(
    top: 20,
    bottom: 30,
  ),
  child: Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Image.network(
        'https://image.flaticon.com/icons/png/512/888/888859.png',
        height: 40,
      ),
      Image.network(
        'https://image.flaticon.com/icons/png/512/919/919828.png',
        height: 40,
      ),
      Image.network(
        'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/1200px-CSS3_logo_and_wordmark.svg.png',
        height: 40,
      ),
      Image.network(
        'https://image.flaticon.com/icons/png/512/226/226777.png',
        height: 40,
      ),
      Image.network(
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPEAAADRCAMAAAAquaQNAAAAzFBMVEX///9BuIM0SV5CvYUzQFw0Rl09t4EzQlwss3pCvoU5tn80R140RF0zPlsqs3k0tX1As4Gz38o6gG81T2Ce17wsQlnD5tXz+vc/qn7K6dpsxZxSvY1Ar4CS07Q6hHDt+PPb8ObX7uM8lnc2X2Q4c2s9nHmq3MQ2ZGYYNU85eW01VmI3amg+o3y849A4b2k7i3Nkw5eQoKXk6urEzs8jO1SZ1biAzKhugYrP2NlLYnCwvL4hQVWImJ5jd4E9V2aksLTb4uJUana3wsRkeIJx1nO9AAAImklEQVR4nOWda1/TTBBHW2hL7RVFqKgIini/oCLe79//Oyk8LA8tm8mc7E52U8572J1f0vzTOZ2kdTxoE1ZvrK1oGX5vVWNjqF5j7cYq2v/guDWbor9otzfUu1k5elOp4DdH+iU24O6ns1brJit59UFHv58nswoFv32iX6DzgB3i6c2TFdCftNvX9rvqDW1+qFDxn031/+/uX4O7P11ha8L+6BY5yB9xwR/JIb7Ftt7f+m+NOz30Z6sPR+otDT/hij/pL1ujh+yc7h2erbEHD/K6/rReOfoMC/4MLlvddbbxyZ5bhSbUPX1CrXyBFX/R/+u11/CytXO+ymwKz2uSUF9RwV/1l63hBjynpxeS4xFMqANw8Tp6Cwp+C87p8QE8xI8uroT+9F/Jj0FC/QEVfwPJ9JgVfJZMjm2aUOCTDBLqHUimNZhMk+35tW7Di9dTkFD62+vv4Ib6KfwU315YiyZUmyTUD2XBP8CneAj3299dXG0HXrxek4TS3V7PQDJ1YDINdi4vRxPqmf4M3NQlFEmmZwHJ5LjeR/+jfTDWH5EnPxUF/wSXrfELttn+dd+K9PaaJNQvRcW/QDI9h4f4jndFmlB3yXeod6UFk2Tq3GVbXUwmx214kElCbZRWDO5cR7DVcymZHLvwk9zW77E8oUgyrcB9Xk4mxw68DXkNzuuShELJBFs9nmQ6Z2CYUHID6ANIpn14Tg+EdWlCvYiVUPUnkwMm1LXnkRKKJNMr1s0rSCbHfXiQUUL9Llz2N/nOBFs9/ftixa1j+EkGimKlOKGIhLgXKZkcu/Q7FGkAFSWUpYSYFCaTw1RR+BtAphLifVnBvAEUnlAgmbowmRZaPX5eWiaUrwFEJMSYSoiXiopbhzChXoGE8ikKSwkhJ5ODNoDu6r9Q+BRFPRJCxlBRDC8pitkXu2QaHOsKbu3WKdGRhID7Kv7OtAhVFAEJRZIJS4ib2oJtJfq8oiB6/HElPa5jCyZUZYlOWj04mbZAxTShkES/qCiAhBhRCXFYWJ2PmiQ6khBGyeQ4tlQUbhHS6qF6XJ1M55uhDSCQUOeKoi49rqMGiU5aPZ0DtJ0FPa6DrcAUxbfTFYgehxICJZPDVKKfKIo69bgOQ0Vx2gAiEiJUj+vYo4qCfIf6gZKpC3dCk8mBFQU4rzd/6j/EMfS4EkOJPgRfErker1owl+ggoQCdqBJCxlBR6Ok+jyohZCwVhZrYEkKGJtQNcL1WEk2P68CKInrBWI+XSwiZ94YNIBUWEkKmB8/rfX3qaKASotcLLbj1Ep7XRFEooHp8opIQMoaKopwR1eOs1ePnPlUU4F6zvGLa6glKJoflFEUJZhJCZmYo0WW4hKgyQ+fBUqKLVJvRiwFalikKiYozejHAc35xEopKiAmSEDKGiqKYyjN6MbBUFIVY6XEdNKFIA6gAPKMXJ5kcfM4v9PYaS4hBpGRyYEURevEKm9GLAVr+X0IBReED6/GQVo8fS0XhoR4JIYPn/EJKrklCyGBFEXTtgmvpf9VDsJyiWKA+CSGCJTpQFPNgCRE7mRwwodCc3xxxJyFCoHN+FRVF95XJ702rgOf8ql2uR3RGL0qrx08tioJKiEitHj+Wc37/A9ewSSbHe5hQFRpA9UsIGfodCisKLCGkGb0Y4CkKepCpHo8hIWSoRIeKovswgYSQoRJ9nR3kTq16XIepokgkIWQsFUUyCSFjqCjSSQgZtKmThNJevAIfx2SHmaJIKSFkqERXKooRTKaBfTI5jBRFWgkhY6IozCchQqCKoq1oAEV5HJMdBlMUneQSQgZtTqMoqIQwbPX42Y4950cfx9Q3kBAyh3EVBZUQAwsJIRP7N6rwv4X+3rQKUaco1iI+jsmMmIoiGwkhcx0qCuE3qvlICJloiiLSgwLtiaYo1jKSEDKRFAV+J0T9yeSIpCjwOyESJJODJpS3ATTOTELIREioBJMQIURQFPlJCBmaUJcUhdHjmOwInqKgM3rpkskRqCiylBAyOKHmFQX8vWnSZHIEKYpcJYQMbPJdVBTZSgiZgAc9VX1bUWoqKwqaTDVKCBn6G9V1d7nGM3p7qUt1VFQUWUsImRlOqJPba9rqaU+StHr8VFIUuUsImTto76cPespeQsjghBqvjLOXEDJYUYzzlxAyWKI3QULIwLcltWEUJ5EQMlRRMBJJCBk6RYFIJSFkYAOIkFkyOaiiAFjM6MUAKgo9JjN6McASXUt+yeSAikJLWgkh07Y5r1OXJUAVhQrdOyFSYZBQmSaTgyoKBfm0evzABlA5GbV6/OB3UZSRg4SQgYqitOAsJIRM1IKzTiZH1ITKRULIQEUhkY2EkMGP0Sgm92RyREuo7JPJgRVFATVPQoQQqQGUl4SQiVJwI5LJQef8vOTa6vEDH/TkI9tWj58ICdXfS10EIzihMpQQMnjOb4GAd0KkIjCh8pQQMkENoMxbPX7onN/8IW5UMjkCFEXDkskRoCjylRAylRVFzhJCZAbfReFIPAkRAn1b0hlNTCZHpYRqZDI5KimK9JMQIVRIqMa0evzgKQrrBwXaA9+W1AwJIYOPceoNBwMVReoZvRighGp0MjlQQjVFQsiABlDDk8mhl+gNkhAyaoneJAkhoz6rU280GsopimZJCBnVFEVD9LgOlaJYjmRyKBJq2jQJIVMu0ZcmmRylimJ5kslRdoiX4YZ6nhJF0UwJISNK9IZKCBlxzq+pEkJGUBSN0+M6iuf8zN8JkYpCRdFkCSFT0ABawmRyFCRUHe+ESIVXUSxlMjm8En05k8nhURTNlxAyHomeekvGXFIUec/oxWAhoWp4W1FqFhTFcrV6/Mw1gJZEQshcVBS9/Gf0YnBBUSx7MjmuTjI5zhVFfW8rSs3ZnN8VSCbHWQPoKiST4zShlkxCyJxMUSydhJB5NF1CCSHT7l2VZHJsL5Me15HsZ/J/AfeG+0UMUywiAAAAAElFTkSuQmCC',
        height: 40,
      ),
      Image.network(
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAMAAABlApw1AAAAmVBMVEX///9AxP8BV5srt/YqtvYDWJwCV5sEWZwCWJwst/YIT4Z/qs2f4f9VxPc1erDP8P9v0v8YiMra5vDv9PgQYaH2+fslb6kZZ6RPi7pflcBnmsOXutavyuC3z+M/gLOT2vrN3uufv9nB7P+B2P/Z8/+I2v/u+v9kp9M8ib88kcc8mtI2otwujskddrSKzfFyuOB1nb1Sv/NeteNMPZQcAAAGmElEQVR4nO3ca3fTOBAG4MQOSUMJi9o0lN4p7FJadtnu//9xG0dxrMvM6OKRlJ6j+VQSm/M+feVcbMNkUqdOnTp16tSpU6dOnTp16tRJM4/33z6tptPpt8eL0lEi5vufU2Xu/yqdJ3D++DQ15sNbasGOv53V99KxfAeM381j6WReg8bvllHpcO6h4r8BgSP+sQvc8Y9a4BX/eAW+8Y9UEBD/GAVh8Y9OEBz/uAQx8Y9IEBn/WATx8Y9CMCp+ecHY+IUFDPFLCnjiFxOwxS8j4IxfQMAcP7eAP35WQZL4GQW3Pz6yz9PP55dlLsHd6mezn/lhFrs5CRy5127/9efN00sWwUOzmv0y8mPxF8ZQhA7x9z8ZAJdbgBTo+R3ZUYUimM/Xv5PnP2s6wEyQ+fH0gEETzH+kBnyRgJlA4w8531kDGzTCadr8Z4s9oBeYafDwJqKM4KrpAVJgRHGm1wwFBOeLAdAJouKjhAyCroADYCag/D7xB0Jmwa6AAdAJIuMDhByC00YHzISePyT+gZBRcL42AQdBVH5C0CQRyALmKkAK6PjOJxeZBBdr+QFCA7QCz7+wpqzgev8BSAW0bSvgaHZ6xGAK5qkEF59twDZ/270W+aaHDYpArYBb0BegAHb5t8dBUHyAkEewAQHySA6LbxNAQcMr+Hr4CrAyCtAFfvkBgX0Y8Ao2FmDIPwh845sErAI+wc3wHWxlLKDdjyI4PyWY8ws+Dt8hV0B+KQjLn1Vw25gAZQG1rRSE5rcEIIBHcDcAFiujgHY/IhywcFTQcAnUAiTAzs8oMCpgEKgF4AAGAVzBaMGDWsAOAObff0eLFuAVjBVcqgXoAC0/JNC/sTkFSAXjBGeNCYAK2L8fQOFpBFlBwyH4ouXXAHoB+nFgxkcJQAULTsHZQgOcrIgCBoEZmSJ4VRAvuGpQAJB/L4DSogK4AhMQKzjXC8AB+5+Wy/5si53UKaDWUKzALOBkBaygmQpYCmytYATPCqIEsgAYABcgBXB+Gc+zAhsQI5DnUhwAI/9uFQUJfAHNv8EFrM38HQBYQTogVOC7hpr34wsYAMARsFyOFTAD5MksD4CZ3yXIBbhuogGEAKggEUCezEIA5ApyCogKGAF9AY2SfwvwKoASBFUwBrABCggAkIK4NRQG+NqMBKCCTIDNaAAmyAO4acIAQH5/QQrARz8AVQAqyAG4bVgAsABfQ3yAOyYAKMgAeGi4AJAgA+CSDwAI0gP2n6N5ALYg/UF80/gC6JdRRJAecBUOCBBkeCPbMAOW5jm71IA1N0AT5AZEfpxGBTk+Tm9sQOgXGlyQ4wvNnRMQXsFSOW8at4JiX4W4AHsBXQAT4DYMECIACkgAuEBfhrxObBECMj/jWYnLBhH4nFqkBEB+/1OLAYCzd/6AQIGVPwlAXlkiAdjpdY/jIHIFBQHkpSUT4HGBI1wA5h8NmNyCAuclpnCBfwGBp1WuXYBYgXFNX83PCuhPrutriL7M6kHothDOAlgAoIC80O0hkFsIOL+jgOgLHAYAvdWgdRIOGwiiAEbAIJj3AuJmD7dA2Vp4FMAAMAXU7TbGHy1Dq28r9Pw+BcQADoK+AhzQWg8MCP1RuaEw8zsLiAIYAvSWM0QATb+ZAPMTBcQB9EuV2E1/SjQHQdlGkAuIDdD/ywEVYN526V+CtoGgCuADaALwxlfrF4wYrCdFUP5ogCqAbj0GU1oG8HFh50ePgBEAKdAAhODw8OEZ64FhU6EdAHQBIwCDwL79nvptmwNsJ/wLGAPYCSwALoAMyDYCys8P6AWvS1+BwiCfnAnf/OMAnUAHeAmIOewqrAMgCUAKXs0jkloiXvG3+wm/AsYCdoIXpYExAn0v4ZV/NKATPE/tAzOcYP0Fwif/eMBWIF5nlMCLAO0unAcAC2ByujYAlsDvw5y9s3Dn5wBMTp+tV3g7F2qgdhTO/CyAyelyZg6QzeetzXxQuPLzACa/sTdZ+2FqgF2EIz8TYPIfFieAAG8v6PxcAFAQREC3FXkAk3siltNAbvYrDwARqDdQ0OnxLUgBHwATmOeJPJ/yFTACUIH7vZhK383Te3wYAZMPaf6vre1wpiwiyAVIJsgGSCXIB0gkyAhII8gJSCLICkghyAtIIMgM4BfkBrALsgO4BfkBzIICAF5BCQCroAiAU1AGwCgoBOATlAKwCYoBuATlAEyCggAeQUkAi6AogENQFsAgKAwYLygNGC0onb9OnTp16tSpU6dOnTp16tR5I/M/hpw2eJQ72pYAAAAASUVORK5CYII=',
        height: 40,
      ),
      Image.network(
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA1VBMVEXy8vL///8zMzNtp11moGA7hzn09PRjnVf6+vr39/f39fcYGBj7+/vi4uJVo0QsLCwmJiYdHR1xcXHPz89cm1Wbm5uiwZ/U4NOjo6NurmNOkE3l6uWAgIBinly+0rwjIyNlZWVWVla7u7sAAADZ2dkQEBBvb2+xyq+ArnvIyMiwsLDf5t6Ojo5bW1s2NjbF1sOfv5xDQ0NhoU5upGiSuI6GsYJXl0m2trZLS0tGjkG2zbSVupGFs3lbnkh4qG+IiIhLmEAffR0ugixuoW2lwaSAq3+Os45+nMysAAAQa0lEQVR4nNWdeUPiuhbAA2JsCj5gpojvYsF1UFFxxW1mnJnru9//I70k3dI2aZL2FHrPHzOK3X6cNclpi1prkJ5DCMFcEBX+AyGO01vHyVGtR3dIwFQglLVe0toIKZyGLcVJnLoupBZCxwZOwKyFEpywZ6W7nBBwi4UlLKm8WlUJSAiCBw8JRdgjYHiBgJkrDCGg+hIBUiQEIbT6EiEAV1eZsFeH+hLBlY21ImHNfBCMlQjXwFedsQrhevg440YI64svMikfc8oSOmvlq8JYjnBNDpiRcu5YinC9BppIKTWWIOxtiI9JCTXaE27EQGOxV6Mt4SYVGIitGi0JN+WBoliq0Y5wsxYaiV3+tyHcvIVGYmOpFoRNsNBILCzVnLAZFhqJuaUaEzYL0ALRkLA5LpiIoTOaETYR0BTRiHD9AwkzMZqqMiFsUhBNiwmiAWFzAY2yhp6wyYAmiFrCZgMaIOoImw6oR9QQNh9QG26KCZuaJtJSjFhI+O8A1KT+IsJmVjIyKUu46eu2kHKETRtNFEnBSENN+G8CLEJUEm46T9h+wcqAqiLcdBjFu7Yr56qAqiKs57rNxTk8QJaMdoQbd0Lny7A7QnadYzaEm3ZCRthvz9r7xOZK5BWqlLABqZ4Rttuzm2fHglHqilLC+i7cWALCdnv4cmGxWGlKuHEnRAlhu989/GrMKLNTCeGmEwWXmJAxfjFOHRI7lRDWe+mGIhBSxuEHMvzeTQibYKMZwnZ7MBuZ7Ze30xxhA+Iokwxhu32ya/bV6wlrvnJTyRF2DQlzeT9LuPlcH0hpwlwJniWs97rNpTxhjij9azPCDKpESIoIGxJmUCVCVETYGBVWIsRqwuaosBIhUhM2R4XVCLGKsEEqrEaYKk9FwgapsCIhlhM2SYUVCZGcsEkqrEqIZYSNUmFVQiQjbJQKKxMSCWFd11pOqhKiPGFTBhWhVCZ0coQ1XWlZqUyIs4TNijMAhHHWjwgbZqQAhCRDWM91lpfqhChN2Ig5UlEACJ0UYbOSIQIhxCnC1F+q4VbcO9y9JGFqG5FQMFLs7KIKYQe7Y7c8Iz05Dk5eihA7p+KKoyMQJh8Tsj+7eS57bxp2F99//CzNSPBV9+6Yn7wEIXbQwdm1sIyDBcJko4u7Wbs9PPpa6v5zd/J+v7W1s3qcuCX2xs5ze8bWmk7pye0JCd6fDejuB8kyTkIYGinunR52+eHodrar6JTPnW9x2fFWc9eWEfe+Hg2Dkw8/ELElxL1jphu2+2wUrRz3YsLgA4I+zoQlrX1s5Y7YXW7db4WIO6udpZWpUgf8kpx8MNtvWRFSB7zuxpvO2seBekhMyH8jl/2BeMhZ6BGGfIvvER8jpIw/F+aMhIwGKaLZy1EasIiQquJjmNo9sPTATENC3Lu4G7azB70+NTRVF+392BJkhzO+G7ojdo5vZplz93MXoySkwXEwyO5+9sEyQkToUBv51s0dM/QIgyDtTrfut3KEO55n4o7Uwg5zX25eVIRUNy+y3Qf9S8KfrIH4uOJKxhdsp234oBnwIc0XI+6sOjp3ZBamOrkBoUo3TIYvF5EOMw6YltndRaGpupOnHF+CuLM6L3RHmYVZEBJ0NVR/Pf3ut4Bw96Tw0HQ7daMAdUAZX0JIM8c7Upkqbl28ZB3QghD3LtvFX8/ZLid81p2k372SF3J5B5QgKrMjc0AjA5UT0gQqdUBRhsec8EN/lkH7Mp85qAN+V/GJhNRUHySFXLGF6QhZAtV/PYMrTvhicp7hS7ZvR+GAUsS8O9L0a+iAUkKCRybq719zwjOjM7C+nZSpKhxQTkhN9VYc2hC9hRURShKoYi9GuNvVbxgwnp0mJ3HzGaIYcWflJ85I9k8sDDRH6ByYaYXtRQmPTaNZe/hVIPyvDjCH+JdAOLKy0BxhtmhVX/IFJTQ/WZrwhyXhahOEg31K+C27dV+1e5rwP5ZK3N4EYf+AEmZDafdQFQQyhHZKfDUjnKn+YEQ4vMtE2P5LC/XSH81ujnvOvryMyxBaKdHbNiGkJeLVTH71BoRs+iUaRkdyQwdOYijtd0ds3EvQwZnkGFlCPaKoQj3hYEhH3apSWkvYZ53v4VSIcM0YnXaFbaJWVdwTx8xKQm3C2BJUqCOMZk4UwyENYXLttBgQzKC7iy7iow1ehFEEG5VmD5MjNLfTbS3hMBlu56Yb9IS05OLfDiucUgXd8CuK6+7+R/r5lYQcZo6TJzQNNistYXrKhA4aswzFhM/823Ent7wAZkV5uMXsGO1H5xrsZ8YPzoGW0FSJ2zrC2XF65QS3sl9vEWH/iE01uXju+V4wk9mLTjDYR1fRxqUIzZT4akCYPbkdYY9+KcsHr9Pp+N6neILBKDGHUoRGwcbbXgMhHq/8Dhfv001O0L9C36oRmmSM1/UQegFgx08RfkHXFQn1Slxtb5CQpqCXaLOShAZK3BwhrdAcdFeRcHulRdyUlfIVDIxuqhH+2t7WEm6tNdL43pwThlVOZUJ65a+GnlhztnAXTx5l9B6WLjvByVE0sVSNkBugZ6bEYsL0PJd1xsfB5Lvnz7HLT3AZL0fEm5Uh/BXYn5bQ01Ztg5GwmMcWMrMbFFZtNOsxIBdPo8UgYS2ikg7DKGkWbIorb3a7aHB4inqQH0AV16VsHYZdvmwFoQphqELDYKMbHw4DzyFEOgDXjQ/VN5tWyRYRoFmw0Y6AefQLOgnsCVXrK7gK4WtMaKREg1mM/mCkWiwzmMXoDyXrKxjFq8nWhL8SQCMlGs1EDeR8hjNRg8FldkUXozgqWxNui2KQMdYymzg8yqyvYPSlLOGvFKHeTtc0I9w/S98XjZPx4TBPmLmMNGEa0CBjeJUIz0TC3Cx2inEm5laCrqJhxvVp1oKz60MiIXn8vW2nxPsngfBZNltZJLO2sHRFRsW7syFFtDUJ52mkzTM4M+clEiJ37P9JERYHm/utpbgOTHYtln8DtYiXp23fGB5FM3cOYktPg5miAYqkujRShLQMfNtOMe4UAQblorB3T5H3ZHxnB9lMJ2vBSe8T9a056GLYH6qb2FLdHGlCViL98+ePiRLvZb1DitpFpg/ZYyMIGSnm/0OhamOFXA+dnlyfFnV3CVPQWULW6/W34I6KjHGvasdMNdKpZNZ/ljctpVvhpLveXfRQC+FnXQs0iZo6coR8xPJXoka5A06VDTVJM6RK+sORuoEQ59ZhsnuffMMtZHIfQuiOEkLecxmbqiRj3O8pu2n4RRb6U2ErD99dNv8v7H6FGKFJb17gjlJC1jX0z2+FEu+ftM176YWUlJg8m0bdsxJ8PaaEvHv45UROyNzx4bck2NzL2mgkR96VDAeTIZ9+d2niGQbrTIZWyoU4+wpC3j77+ieXMd7yfFiGLPMnZR+WbPd84om/HkIJzW8mKWrEdN1P7o5Chsg7IFX2o1St2HlOuWM/aoE1vLBM4ul3P6Kvx2EdQ8YHKhZ38r/fcbC5/y5pSXTRXrw4lLtIsbZgYd6cj4kjrlp3hfzXAiRkmYMWckGGkLSVYnfq+8F8piy8JuuaBj2tkt17p9eBpbNOhOTknNDu2yo8jfv251VSovE/jX/yxS82pSnvqw1qC0mJZnhy55jdzHA2Ev0Xc0LIG/No5vghM0N38sinaztvnNOT99US57JfXGEVCi3ksg/NIpwQ9L417E4kDog/GR9bvHTdN5//KO+rJXYPEsye3Mmq3wm6oMsfUnqa3AfukiqvE8UYF90GuBJbrnpfWG73Vh2E2XO6i3NumHH9jYUVBrgYIJeQsM7TuJN3rjF/Kpgf1eoD1+qTxV0nZQRH91vUdgbeHsFQsvU3/UPgjreFhXlVccS7gmqQQlXFypXUdmDSqpWQOyB3N3n9nXdQeMncuwYrcch8U951mwmy8JLcuwbviGGJpvOz2E8/ZZmjsiT3H4KbaVCi+YrSJbVlVOwsa0CU3gcMIni5Ms53UcG6+gRHFO8DBjZTd0qvWV6zyC7EnXb8jr8HTijeyw1spozQWyiuGGOXiRh/XNSpg7DGZypwwolYw7iRvbruZDydz+fTcWrNvQ7C9DMVYM00Q4gXj++PC/YrxtNzj4rv03+ehDxYB2H6uRiwZpolHK98b8y6QRYPXtgiyQf7T/FYoA7CVpoQNOnnCOmvlBBPgmkMn6uR/fRQI2H2+TSgzxhSELrnPIc8zqfL5XTvgaX69xCrBsLsM4ZAY42cEE9YFbDkoZT+gx9ZSlkEW8ET5p4TBRpr5ITukv43jTkwUynrI2QCT5h/1hdkrFEQztNZkn5Mi+66CFt5QsBYoyCkn/pPEyHZv+/tvdVEKHvmHqASFX44DobDb2MU+CL/N9gEnLAlI4SLNaps4QVp0PMezt/ny4VbX8aXPvsSMGGossU0zvc+S4r+XjzyhSZsSQnhlKggpDXNOy/ZkqrmrR4rVTyDFk6JKkLmf+O32/OHmNP7rCXSqJ4jDKZEJSEKxxmTxXLvJzfZMOXDEiqfBQ2mREUsTUIn/3ny7sVgsIQ9JSGUEqWE7tJ/6MwFDOw+0qH9EzxhwTPZoXKinHDsRTzRZvN6CFtFhDCFjcIPmduNBQ5WmPpzcMLCdyMAKVFRtd2y8dLUjeuZTza4GINHmixR5ncQJRaMnjqe/3TLB4iffE3jHDxb6N5RAqJEVU2zXMX1TJAQfX8CPT7UvmcGJGMoa5ql73WSeRphogaOMPdOq1re96TM+O7ks+OF4j99JnP+YIQG73uCsFN1TUMDzGTBZIKSOVRaAIARSnDyH1Wfz+Bj3aTZIlW1UZxAhM3xJ9Ssvtl716rbKV+3SFZ3+QTNWHXUcBXRuwUgNHx3XnU7xanVXez+9JWEwkoxQACQwsg+rBxPxdVdFz0xAvmGBivFFmL+DkuAvB+v7p5zFflSG8RRgxRMR4bFe0hhUkawuhtO5EuanbD5SrGRWL1LFqS04QR+kNsXeQR3AtwZpSJRfA4yBY7dKZ+wuM1rkHfTKrvbyojtO52hxlEswQdP/hEhWasGN1DTB/Hqxfq93GDjfZ7b2QxUYozUfINscg7XK1Ti3eqgvcOTObXWqPcr7PdSdAqXPIMao4AQbpqfZg4vSgr1NAkVURT8DXASPEzstJALm/lgG70KIAoJAdcU467ZDv8XuFlPFUb1hJDLpmErJp/HUN/NVkqUYdSAELSBIajRfG8PuEuvGFBHCNyjgW5X+rvZLEUDqCWERZTeq1BN5OW2DSFwey10N7AW0ICwcS+7EkVnomaEDUY0ADQibN77rkIxATQjbNx75wIpTPSWhI1ENLtyU8LmvbesYDRRkrBhiPosYU/YqJBqFGOsCRsUUs1ijD1hUyzV2AVLEDbCUs1dsAxhAyzVxkLLEG7aUi0VWIpwo2q0VWA5ws2p0V6BZQk3U8TZhdCKhJsIqhZJHoRw3aZaykArErZKP79inXyVCNfGWNIBIQjXwohLZAhAwtoZq+kPhLBVZ1yt4n+RQBCyN5nWgIfL5oe0wBBSY4VWZFX3iwWKsAWqSCD1cQEkbAFBQuK1oAlbzFwrURIo44wFnJBJSVUCKy+UWgiZOFa6xKQWOia1EXKhmDpOTOHALVOUeglD6TmEkKQzmP9AiFMvWST/B31iukysOhiOAAAAAElFTkSuQmCC',
        height: 40,
      ),
    ],
  ),
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'My Resume',
        theme: ThemeData(
          primaryColor: Colors.grey[300],
        ),
        home: Scaffold(
            appBar: AppBar(
              title: const Text('My Resume : เรซูเม่ของฉัน'),
            ),
            body: ListView(
              padding: EdgeInsets.all(50),
              children: <Widget>[
                Container(
                  width: 100.0,
                  height: 300.0,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage('images/profile.png')),
                    borderRadius: BorderRadius.all(Radius.circular(8.0)),
                    color: Colors.black,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.7),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(10, 5), // changes position of shadow
                      ),
                    ],
                  ),
                ),
                bio1,
                bio2,
                Divider(),
                // textSection,
                section(
                    'https://image.flaticon.com/icons/png/512/584/584574.png',
                    ' สกิล'),
                skillList,
                Divider(),
                section(
                    'https://image.flaticon.com/icons/png/512/1077/1077114.png',
                    'ประวัติส่วนตัว'),
                Container(
                  padding: const EdgeInsets.only(
                    top: 20,
                    bottom: 30,
                  ),
                  child: Column(
                    children: [
                      profile('วันเกิด', '19 สิงหาคม 2542', 18),
                      profile('โทรศัพท์', '098--------', 18),
                      profile('อีเมลล์', '61160142@go.buu.ac.th', 18),
                    ],
                  ),
                ),
                Divider(),
                section(
                    'https://img-premium.flaticon.com/png/512/3074/premium/3074094.png?token=exp=1627211842~hmac=d7bcbaff2bf262dcb085524eb25cf46c',
                    'การศึกษา'),
                Container(
                  padding: const EdgeInsets.only(
                    top: 20,
                    bottom: 30,
                  ),
                  child: Column(
                    children: [
                      profile('ปริญญาตรี', 'มหาวิทยาลัยบูรพา', 18),
                      profile(
                          'มัธยมศึกษา', 'โรงเรียนอิสลามศรีอยุธยามูลนิธิ', 18),
                      profile(
                          'ประถมศึกษา', 'โรงเรียนบ้านพลับ (วิสุทธิวังราช)', 18),
                    ],
                  ),
                ),
              ],
            )));
  }
}
